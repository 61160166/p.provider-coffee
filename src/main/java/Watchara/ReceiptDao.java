/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Watchara;


import Narathip.Customer;
import Ployrung.*;
import Sarunporn.Employee;
import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import poc.TestSelectReceiptDetail;
import Watchara.Receipt;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author ACER
 */
public class ReceiptDao implements DaoInterface<Receipt> {


    @Override
    public int add(Receipt object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO Receipt (Total, Discount, Employee_ID, Customer_ID) VALUES (?, ?, ?, ?, ?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setDouble(1,object.getTotal());
            stmt.setDouble(2, object.getDiscount());
            if(object.getCustomer() != null){
                stmt.setInt(1, object.getCustomer().getCustomer_ID());
            }
            stmt.setInt(3, object.getCustomer().getCustomer_ID());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            
            if(result.next()){
                id = result.getInt(1);
                object.setId(id);
            }
            
             for (ReceiptDetail r : object.getReceiptDetail()) {
                String sqlDetail = "INSERT INTO ReceiptDetail (Amount, Price, Product_ID, Receipt_ID) VALUES ( ?,?,?,? )";
                PreparedStatement stmtDetail = conn.prepareStatement(sqlDetail);
                stmtDetail.setInt(1, r.getAmount());
                stmtDetail.setDouble(2, r.getPrice());
                stmtDetail.setInt(3, r.getProduct().getId());
                stmtDetail.setInt(4, r.getReceipt().getId());
                int rowDetail = stmtDetail.executeUpdate();
                ResultSet resultDetail = stmtDetail.getGeneratedKeys();
                if (resultDetail.next()) {
                    id = resultDetail.getInt(1);
                    r.setId(id);
                }
             }
            
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectReceiptDetail.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return id;
    }

    public int update(ReceiptDetail object) {
//        Connection conn = null;
//        Database db = Database.getInstance();
//        conn = db.getConnection();
//        int row = 0;
//        try {
//            String sql = "UPDATE ReceiptDetail SET Amount = ? ,Price = ?,Product_ID = ? , Receipt_ID = ? WHERE ReceiptDetail_ID = ?";
//            PreparedStatement stmt = conn.prepareStatement(sql);
//            stmt.setInt(1,object.getAmount());
//            stmt.setDouble(2, object.getPrice());
//            stmt.setInt(3,object.getId());
//            
//            row = stmt.executeUpdate();
//        } catch (SQLException ex) {
//            Logger.getLogger(TestSelectReceiptDetail.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        db.close();
        return 0;
    }
    
    @Override
    public ArrayList<Receipt> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT b.id as id, created, Customer_id,"
                    + "c.firstname as Customer_FirstName, c.lastname as Customer_LastName, "
                    + "c.tel as Customer_Tel, c.point as customer_point, User_ID,"
                    + " u.username as UserName,u.tel as User_Tel, b.total as Total,"
                    + " b.discount as Discount FROM Receipt b,"
                    + "Customer c,User u WHERE b.Customer_id = c.id AND b.User_ID"
                    + " = u.id ORDER BY created DESC";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()){
               int id = result.getInt("Receipt_ID");
               Date created = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(result.getString("Created"));
               int customerId = result.getInt("Customer_Customer_ID");
                String customerName = result.getString("Customer_FirstName");
                String customerSurname = result.getString("Customer_LastName");
                String customerTel = result.getString("Customer_Tel");
                int employeeId = result.getInt("Employee_Employee_ID");
                String employeeName = result.getString("Employee_FirstName");
                String employeeTel = result.getString("Employee_Tel");
                double total = result.getDouble("total");
                double discount = result.getDouble("discount");
                Receipt receipt = new Receipt(id, created,
                        new Employee(employeeId, employeeName, employeeTel),
                        new Customer(customerId, customerName, customerSurname, customerTel),
                        discount);
                getReceiptDetail(conn, id, receipt);               
                list.add(receipt);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectReceiptDetail.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(ReceiptDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return list;
    }

    private void getReceiptDetail(Connection conn, int id, Receipt receipt) throws SQLException {
        String sqlDetail = "SELECT rd.id as ReceiptDetail_ID, "
                + "Receipt_ID, "
                + "Product_ID, "
                + "p.name as Product_Name, "
                + "p.price as Product_Price, "
                + "rd.price as rice, Amount "
                + "FROM ReceiptDetail rd, Product p "
                + "WHERE Receipt_ID = ? AND rd.Product_ID = p.id;";
        PreparedStatement stmtDetail = conn.prepareStatement(sqlDetail);
        stmtDetail.setInt(1, id);
        ResultSet resultDetail = stmtDetail.executeQuery();

        while (resultDetail.next()) {
            int receiptId = resultDetail.getInt("id");
            int productId = resultDetail.getInt("Product_ID");
            String productName = resultDetail.getString("Product_Name");
            double productPrice = resultDetail.getDouble("Product_Price");
            double price = resultDetail.getDouble("Price");
            int amount = resultDetail.getInt("Amount");
            Product product = new Product(productId, productName, productPrice);
            receipt.addRecieptDetail(receiptId, product, amount, price);
        }
    }
    
    @Override
    public Receipt get(int id) {
//        Connection conn = null;
//        Database db = Database.getInstance();
//        conn = db.getConnection();
//        try {
//            String sql = "SELECT ReceiptDetail_ID, Amount, Price, Product_ID, Receipt_ID FROM ReceiptDetail WHERE ReceiptDetail_ID=" + id;
//            Statement stmt = conn.createStatement();
//            ResultSet result = stmt.executeQuery(sql);
//            if(result.next()){
//               int recid = result.getInt("Receip_ID");
//               Date created = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(result.getString("Created"));
//               int customerId = result.getInt("Customer_ID");
//               String customerName = result.getString("FirstName");
//               String customerSurname = result.getString("LastName");
//               String customerTel = result.getString("Tel");
////               String type = result.getString("Type");
//               int userId = result.getInt("User_ID");
//               String userName = result.getString("UserName");
//               String userTel = result.getString("Tel");
//               double total = result.getDouble("Total");
//               double discount = result.getDouble("Discount");
//               Receipt rec = new Receipt(recid, null,
//                        new User(userId, userName, userTel),
//                        new Customer(customerId, customerName, customerSurname, customerTel),
//                        discount));
//               return rec;
//            }
//        } catch (SQLException ex) {
//            Logger.getLogger(TestSelectReceiptDetail.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (ParseException ex) {
//            Logger.getLogger(ReceiptDao.class.getName()).log(Level.SEVERE, null, ex);
//        }
        return null;
    }
    

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM Receipt WHERE Receipt_ID = ? ";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1,id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectReceiptDetail.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return row;
    }
    
    
    public static void main(String[] args) {
        ReceiptDao dao = new ReceiptDao();
        System.out.println(dao.getAll());
        System.out.println(dao.get(1));
//        int id = dao.add(new ReceiptDetail(-1, 2, 110, null, null));
//        System.out.println("id: "+ id);
//        Product lastProduct = dao.get(id);
//        System.out.println("last product: "+lastProduct);
//        lastProduct.setPrice(100);
//        int row = dao.update(lastProduct);
//        Product updateProduct = dao.get(id);
//        System.out.println("update product: " + updateProduct);
//        dao.delete(id);
//        Product deleteProduct = dao.get(id);
//        System.out.println("delete product: " + deleteProduct);
    } 

    @Override
    public int update(Receipt object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
