/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ployrung;

/**
 *
 * @author ACER
 */
public class Product {
    private int id;
    private String name;
    private int amount;
    private double price;
    private String type;

    public Product(int id, String name, int amount, double price, String type) {
        this.id = id;
        this.name = name;
        this.amount = amount;
        this.price = price;
        this.type = type;
    }
    
    public Product(String name, int amount, double price, String type) {
        this(-1, name, amount, price, type);      
    }
    
    public Product(int id, String name, double price) {
         this(id, name, "", price, "");
    }

    private Product(int id, String name, String string, double price, String string0) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", amount=" + amount + ", price=" + price + ", type=" + type + '}';
    }
    
    
}
