/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poc;

import Ployrung.Product;
import Watchara.ReceiptDetail;
import database.Database;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dell
 */
public class TestSelectReceiptDetail {
    public static void main(String[] args) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT ReceiptDetail_ID, Amount, Price, Product_ID, Receipt_ID  FROM ReceiptDetail";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()){
               int id = result.getInt("ReceiptDetail_ID");  
               int Amount = result.getInt("Amount");
               double price = result.getDouble("Price");
               ReceiptDetail rec = new ReceiptDetail(id, Amount, price, null, null);
               System.out.println(rec);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectReceiptDetail.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
    }

}
